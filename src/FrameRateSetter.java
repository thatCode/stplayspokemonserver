import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class FrameRateSetter extends Application{
	/*public static void main(String[] args) {
		FrameRateSetter FRS=new FrameRateSetter();
		FRS.begin_launch(args);
	}*
	/*public void begin_launch(String[] args){
		launch(args);
	}*/
	public static TextField field;
	public static Button button;
	FlowPane pane;
	@Override
	public void start(Stage arg0) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Running Window");
		pane=new FlowPane();
		field=new TextField();
		button=new Button();
		button.setText("Set Frame Rate");
		pane.getChildren().add(field);
		pane.getChildren().add(button);
		button.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int i=Integer.parseInt(field.getText());
				Server.setDelayTime(i);
				//System.out.println(i);
				field.setText("");
			}
			
		});
		Scene s=new Scene(pane,300,100);
		arg0.setScene(s);
		arg0.show();
	}

}
