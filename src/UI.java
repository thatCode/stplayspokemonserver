import java.util.PriorityQueue;

import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class UI extends Application{
	/*public static void main(String[] args){
		UI test=new UI();
		test.begin_launch(args);
	}*/
	public void begin_launch(String[] args){
		queue=new PriorityQueue<String>();
		launch(args);
		
	}
	FlowPane pane;
	public static TextArea area;
	public FrameRateSetter FRS;
	public static PriorityQueue<String> queue;
	//public svtatic TextField field;
	//public static Button button;
	@Override
	public void start(Stage arg0) throws Exception {
		// TODO Auto-generated method stub
		area=new TextArea();
		//field=new TextField();
		//button=new Button();
		/*button.setText("Set Frame Rate");
		button.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int i=Integer.parseInt(field.getText());
				Server.setDelayTime(i);
				field.setText("");
			}
			
		});
		*/
		area.setWrapText(true);
		area.setPrefWidth(250f);
		area.setPrefHeight(380f);
		pane=new FlowPane();
		
		//area.setDisable(true);
		//pane.getChildren().add(field);
		//pane.getChildren().add(button);
		pane.getChildren().add(area);
		area.textProperty().addListener(new ChangeListener<Object>(){

			@Override
			public void changed(ObservableValue<? extends Object> arg0, Object arg1, Object arg2) {
				// TODO Auto-generated method stub
				area.setScrollTop(Double.MAX_VALUE);
			}
			
		});
		arg0.setOnCloseRequest(new EventHandler<WindowEvent>(){

			@Override
			public void handle(WindowEvent arg0) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
			
		});
		Scene s = new Scene(pane,250,380);
		Thread thread =new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(true){
					try {
						Thread.sleep(10);
						while(queue.size()!=0){
							String string=queue.poll();
							
							area.appendText("\n"+string);
							
							
						}
						if(area.getText().split("\n").length>300){
							//System.out.println("doing the thing");
							area.deleteText(0, 500);
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
			
		});
		thread.start();
		arg0.setScene(s);
		arg0.show();
		
		

	}
	
	public void addLine(String s){
		//System.out.println(queue+" "+s);
		queue.add(s);
		/*area.appendText("\n"+s);
		if(area.getText().split("\n").length>300){
			//System.out.println("doing the thing");
			area.deleteText(0, 500);
		}
		*/
	}

}
