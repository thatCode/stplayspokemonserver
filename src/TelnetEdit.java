import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class TelnetEdit implements Runnable
{
	private static ServerSocket ss;
	private static Thread st;
	private static TelnetEdit e;

	private BufferedReader in;
	private PrintWriter out;

	private static Socket s;
	private static Thread t;

	boolean run = true;

	public static void init()
	{
		try
		{
			ss = new ServerSocket(9000);
			t = new Thread(new TelnetEdit());
			t.start();
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public String read()
	{
		try
		{
			return in.readLine();
		}
		catch (SocketException e)
		{
			System.out.println("User disconnected");
			t.interrupt();
			run = false;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void send(String msg)
	{
		out.println(msg);
		out.flush();
	}

	public void run()
	{
		try
		{
			s = ss.accept();
			try
			{
				// Creates the BufferedReader and PrintWriter
				in = new BufferedReader(new InputStreamReader(s.getInputStream()));
				out = new PrintWriter(s.getOutputStream());
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		while (run)
		{
			String temp = read();
			if (temp != null && !temp.equals(""))
			{
				if (temp.startsWith("DELAYTIME"))
				{
					Server.setDelayTime(Integer.parseInt(temp.substring(temp.indexOf(":") + 1)));
				}
			}
		}
	}
}
