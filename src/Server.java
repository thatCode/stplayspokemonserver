import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

public class Server
{
	private static ServerSocket s;
	private static ArrayList<Client> clients;

	public static void main(String[] args)
	{
		Game.init(args);
		try
		{
			// Creates a new ServerSocket, inits the array list, and creates
			// a new client every time the ServerSocket connects, and adds
			// it to the array list
			s = new ServerSocket(9001);
			clients = new ArrayList<Client>();
			Resend.init();
			TelnetEdit.init();
			while (true)
			{
				clients.add(new Client(s.accept()));
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static boolean isIn(Client c)
	{
		for (Client client : clients)
		{
			if (client.getIP().equals(c.getIP()))
			{
				return true;
			}
		}
		return false;
	}

	public static void setDelayTime(int miliseconds)
	{
		System.out.println("New Delay: " + miliseconds);
		Resend.delay = miliseconds;
		sendDelay(miliseconds);
	}

	public static void sendDelay(int miliseconds)
	{
		for (Client client : clients)
		{
			client.send("DELAYTIME:" + miliseconds);
		}
	}

	public static void delete(Client c)
	{
		clients.remove(c);
	}

	public static void gotMessage(String msg)
	{
		String prettyMessage = msg;
		if (msg.contains(":V"))
		{
			prettyMessage = msg.substring(0, msg.indexOf(":") + 1) + " Start";
		}
		else if (msg.contains(":U"))
		{
			prettyMessage = msg.substring(0, msg.indexOf(":") + 1) + " Up";
		}
		else if (msg.contains(":D"))
		{
			prettyMessage = msg.substring(0, msg.indexOf(":") + 1) + " Down";
		}
		else if (msg.contains(":L"))
		{
			prettyMessage = msg.substring(0, msg.indexOf(":") + 1) + " Left";
		}
		else if (msg.contains(":R"))
		{
			prettyMessage = msg.substring(0, msg.indexOf(":") + 1) + " Right";
		}
		else
		{
			prettyMessage = msg.substring(0, msg.indexOf(":") + 1) + " " + msg.substring(msg.indexOf(":") + 1);
		}
		Game.ui.addLine(prettyMessage);
		for (Client client : clients)
		{
			client.send(msg); // Hi
		}
	}
}
