import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

public class Client implements Runnable
{
	BufferedReader in;
	PrintWriter out;

	Socket s;
	Thread t;

	boolean run = true;

	public Client(Socket s)
	{
		this.s = s;
		if (Server.isIn(this))
		{
			try
			{
				s.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			Server.delete(this);
			return;
		}
		try
		{
			// Creates the BufferedReader and PrintWriter
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new PrintWriter(s.getOutputStream());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		// Creates and starts a new thread
		t = new Thread(this);
		t.start();
	}

	public String read()
	{
		try
		{
			return in.readLine();
		}
		catch (SocketException e)
		{
			System.out.println("User disconnected");
			t.interrupt();
			run = false;
			Server.delete(this);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public String getIP()
	{
		return s.getInetAddress().toString();
	}

	public void send(String msg)
	{
		out.println(msg);
		out.flush();
	}

	public void run()
	{
		// Reads to infinity
		// If it gets something that isn't null or empty, it presses that button
		while (run)
		{
			String temp = read();
			if (temp != null && !temp.equals(""))
			{
				Server.gotMessage(temp);
				Game.press(temp.substring(temp.indexOf(":") + 1));
			}
		}
	}
}
