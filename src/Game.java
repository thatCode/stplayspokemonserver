import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class Game
{
	private static Robot robot;
	public static UI ui;

	public static void init(String[] args)
	{

		try
		{
			// Creates the robot
			robot = new Robot();
		}
		catch (AWTException e)
		{
			e.printStackTrace();
		}
		ui = new UI();
		Thread thread = new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				ui.begin_launch(args);
			}

		});

		thread.start();

	}

	public static void press(String s)
	{
		// Key Mappings
		// Sent Key --> Pressed Key
		// A --> A
		// B --> B
		// U --> U (Up)
		// D --> D (Down)
		// L --> L (Left)
		// R --> R (Right)
		// V --> V (Start)

		// Array of strings that represent keys
		String[] controlsS = new String[]
		{ "A", "B", "U", "D", "L", "R", "V" };

		// Array of KeyEvents that corresponds by index to the strings sent
		int[] controlsE = new int[]
		{ KeyEvent.VK_A, KeyEvent.VK_B, KeyEvent.VK_U, KeyEvent.VK_D, KeyEvent.VK_L, KeyEvent.VK_R, KeyEvent.VK_V };

		// Gets index of the string in the controls list
		int i = indexOf(controlsS, s);
		if (i != -1)
		{
			System.out.println(controlsS[i]);
			// Executes the corresponding KeyEvent if it maps correctly
			// robot.keyPress(controlsE[i]);
			try
			{
				Thread.sleep(30);
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// robot.keyRelease(controlsE[i]);
		}
	}

	public static int indexOf(String[] arr, String k)
	{
		for (int i = 0; i < arr.length; i++)
		{
			if (arr[i].equals(k))
			{
				return i;
			}
		}
		return -1;
	}

	public static boolean contains(String[] arr, String k)
	{
		// Cheaty
		return (indexOf(arr, k) == -1 ? false : true);
	}
}
