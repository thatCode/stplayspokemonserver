
public class Resend implements Runnable
{
	private static Resend r;
	private static Thread t;

	public static int delay = 5000;

	public static void init()
	{
		r = new Resend();
		t = new Thread(r);
		t.start();
	}

	public void run()
	{
		while (true)
		{
			try
			{
				Thread.sleep(10000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			Server.sendDelay(delay);
		}
	}
}
